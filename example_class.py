import random
# Every Word in a class name is capitalized
class Wizard:
    arcane = True # This is an attribute, a variable inside of a class

    def __init__(self, name, arcane=True **kwargs):
        self.name = name
        self.arcane = arcane


        for key, value in kwargs.items():
            setattr(self, key, value) # Look into SETATTR and how it works


    def fireBall(self): # Self represents the INSTANCE using the classes method.
        if self.arcane: # This is an example of using self to check an INSTANCE
            return bool(random.randint(0, 1))
        else:
            return False

    def readScroll(self, scroll):
        if scroll == 'arcane':
            return True
        else:
            return False
