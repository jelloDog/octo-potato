Objects are fucking everywhere man, like skynet. STRINGS are objects, functions are objects, IT'S GAME OVER MAN
Think of classes as the blueprints for objects

A *variable name* that belongs to a *class* is called a *attribute*
A *function* defined inside of a *class* is called a *method*
An *instance* is the result of using, or calling, a *class*

Python in all it's clean glory uses *dot syntax* to access members of a class or instance. A *member* is *anything defined in the class*

```Python
# For Example, a DOG class with the METHOD of BARK
dog.bark
# or
cody = Dog()
cody.bark
```

Methods in classes are generally the ACTIONS the INSTANCES of your class can perform.
When they are used however, they are used by an INSTANCE not by the actual class. Because of this methods always take at least one parameter
this parameter represents the INSTANCE using the method (self). Now their is a janky way to use a method, with a class instead of an instance. See Below

```Python
# the DOG class has the BARK method
cody = Dog() # Assigning the dog...
Dog.bark(cody) # making the dog bark
# The given parameter is used so that the method knows what instance is calling the method, without the given parameter it would throw a TypeError
```

when you create a new INSTANCE of a class Python looks for a method named `__init__` the double underscores indicate that the method is run by python on it's own. We don't have to call it ourselves. init lets us control how a class instance is created (or initialized)

***IMPORTANT BIT***
setattr sets the value of a given attribute to an object
`SETATTR(OBJECT, NAME, VALUE)`
object - object whose attribute has to be set
name - string which contains the name of the attribute to be set
value - value of the attribute to be set
